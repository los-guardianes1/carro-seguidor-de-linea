int infraPin1 = 10;    
int infraPin2 = 11;    
int infraPin3 = 12;
int valorInfra1 = 0;  
int valorInfra2 = 0;  
int valorInfra3 = 0;

void setup() { 
  Serial.begin(9600); 
  pinMode(infraPin1, INPUT);     
  pinMode(infraPin2, INPUT);     
  pinMode(infraPin3, INPUT); 
} 

void loop() { 
  valorInfra1 = digitalRead(infraPin1);   
  Serial.print("SENSOR1 ");         
  Serial.println(valorInfra1);    
  valorInfra2 = digitalRead(infraPin2);    
  Serial.print("SENSOR2 ");       
  Serial.println(valorInfra2);    
  valorInfra3 = digitalRead(infraPin3);
  Serial.print("SENSOR3 ");
  Serial.println(valorInfra3);
  delay(1000);  
}
