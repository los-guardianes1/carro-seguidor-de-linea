int IR1 = 0;
int IR2 = 0;
int IR3 = 0;
int motor2B = 16;
int motor2A = 5;
int motor1B = 4;
int motor1A = 0;

void setup() {
  Serial.begin(9600);
  pinMode(14, INPUT);
  pinMode(12, INPUT);
  pinMode(13, INPUT);
  pinMode(motor1A, OUTPUT);
  pinMode(motor1B, OUTPUT);
  pinMode(motor2A, OUTPUT);
  pinMode(motor2B, OUTPUT);
}

void loop() {
  IR1 = digitalRead(14);
  IR2 = digitalRead(12);
  IR3 = digitalRead(13);
  Serial.print(IR1);
  Serial.print(IR2);
  Serial.println(IR3);
  if (IR1 == 1 && IR2 == 1 && IR3 == 1){
    parar();
  }
  else if (IR1 == 1 && IR2 == 1 && IR3 == 0){
    derecha();
  }
  else if (IR1 == 1 && IR2 == 0 && IR3 == 1){
    avanzar();
  }
  else if (IR1 == 1 && IR2 == 0 && IR3 == 0){
    derecha();
  }
  else if (IR1 == 0 && IR2 == 1 && IR3 == 1){
    izquierda();
  }
  else if (IR1 == 0 && IR2 == 1 && IR3 == 0){
    avanzar();
  }
  else if (IR1 == 0 && IR2 == 0 && IR3 == 1){
    izquierda();
  }
  else if (IR1 == 0 && IR2 == 0 && IR3 ==0){
    avanzar();
  }

  delay(0);
}

void avanzar() {
  digitalWrite(motor1A, 0);
  digitalWrite(motor1B, 1);
  digitalWrite(motor2A, 1);
  digitalWrite(motor2B, 0);
}
void izquierda(){
  digitalWrite(motor1A, 0);
  digitalWrite(motor1B, 0);
  digitalWrite(motor2A, 1);
  digitalWrite(motor2B, 0);
}
void derecha(){
  digitalWrite(motor1A, 0);
  digitalWrite(motor1B, 1);
  digitalWrite(motor2A, 0);
  digitalWrite(motor2B, 0);
}
void parar() {
  digitalWrite(motor1A, 0);
  digitalWrite(motor1B, 0);
  digitalWrite(motor2A, 0);
  digitalWrite(motor2B, 0);
}
