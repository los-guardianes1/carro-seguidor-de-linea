/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocarrodefinitivo;



/**
 *
 * @author hojah
 */
public class EditFRM extends javax.swing.JFrame {
int R, G, B;
int RV, GV, BV;
String UsuarioN, ContraN;
    /**
     * Creates new form EditFRM
     */
    public EditFRM() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel23 = new javax.swing.JPanel();
        JsldRED6 = new javax.swing.JSlider();
        JsldGREEN6 = new javax.swing.JSlider();
        JsldBLUE6 = new javax.swing.JSlider();
        JpanelRGB = new javax.swing.JPanel();
        JbtnRGB = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JbtnRGB1 = new javax.swing.JButton();
        JTF = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        JPF = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel23.setBackground(new java.awt.Color(102, 0, 0));

        JsldRED6.setForeground(new java.awt.Color(102, 0, 51));
        JsldRED6.setMajorTickSpacing(15);
        JsldRED6.setMaximum(255);
        JsldRED6.setMinorTickSpacing(3);
        JsldRED6.setOrientation(javax.swing.JSlider.VERTICAL);
        JsldRED6.setToolTipText("");
        JsldRED6.setValue(23);
        JsldRED6.setBorder(javax.swing.BorderFactory.createTitledBorder("RED (R)"));
        JsldRED6.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                JsldRED6JsldREDStateChanged(evt);
            }
        });

        JsldGREEN6.setForeground(new java.awt.Color(102, 0, 51));
        JsldGREEN6.setMajorTickSpacing(15);
        JsldGREEN6.setMaximum(255);
        JsldGREEN6.setMinorTickSpacing(3);
        JsldGREEN6.setOrientation(javax.swing.JSlider.VERTICAL);
        JsldGREEN6.setValue(10);
        JsldGREEN6.setBorder(javax.swing.BorderFactory.createTitledBorder("GREEN (G)"));
        JsldGREEN6.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                JsldGREEN6JsldGREENStateChanged(evt);
            }
        });

        JsldBLUE6.setForeground(new java.awt.Color(102, 0, 51));
        JsldBLUE6.setMajorTickSpacing(15);
        JsldBLUE6.setMaximum(255);
        JsldBLUE6.setMinorTickSpacing(3);
        JsldBLUE6.setOrientation(javax.swing.JSlider.VERTICAL);
        JsldBLUE6.setValue(23);
        JsldBLUE6.setBorder(javax.swing.BorderFactory.createTitledBorder("BLUE (B)"));
        JsldBLUE6.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                JsldBLUE6JsldBLUEStateChanged(evt);
            }
        });

        JpanelRGB.setBackground(new java.awt.Color(R=JsldRED6.getValue(), G=JsldGREEN6.getValue(), B=JsldBLUE6.getValue()));

        javax.swing.GroupLayout JpanelRGBLayout = new javax.swing.GroupLayout(JpanelRGB);
        JpanelRGB.setLayout(JpanelRGBLayout);
        JpanelRGBLayout.setHorizontalGroup(
            JpanelRGBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        JpanelRGBLayout.setVerticalGroup(
            JpanelRGBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 15, Short.MAX_VALUE)
        );

        JbtnRGB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/IconChange.png"))); // NOI18N
        JbtnRGB.setText("Cambiar colores de esta ventana");
        JbtnRGB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JbtnRGBActionPerformed(evt);
            }
        });

        jPanel1.setOpaque(false);

        jLabel1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 204, 204));
        jLabel1.setText("¿Quieres cambiar tus datos de inicio de sesión?");

        JbtnRGB1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/IconChange.png"))); // NOI18N
        JbtnRGB1.setText("Cambiar datos");
        JbtnRGB1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JbtnRGB1ActionPerformed(evt);
            }
        });

        jLabel2.setBackground(new java.awt.Color(0, 0, 0));
        jLabel2.setForeground(new java.awt.Color(204, 204, 204));
        jLabel2.setText("Nuevo nombre de usuario:");

        jLabel3.setBackground(new java.awt.Color(0, 0, 0));
        jLabel3.setForeground(new java.awt.Color(204, 204, 204));
        jLabel3.setText("Nueva contraseña:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JbtnRGB1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(JPF, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                                    .addComponent(JTF))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(JPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 92, Short.MAX_VALUE)
                .addComponent(JbtnRGB1)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addComponent(JsldRED6, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JsldGREEN6, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(JsldBLUE6, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(JpanelRGB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JbtnRGB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(JsldRED6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(JsldGREEN6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(JsldBLUE6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(JbtnRGB))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(JpanelRGB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(178, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void JbtnRGBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JbtnRGBActionPerformed
        // TODO add your handling code here:
        jPanel23.setBackground(new java.awt.Color(R=JsldRED6.getValue(),G=JsldGREEN6.getValue(),B=JsldBLUE6.getValue()));
    }//GEN-LAST:event_JbtnRGBActionPerformed

    private void JsldRED6JsldREDStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_JsldRED6JsldREDStateChanged
        // TODO add your handling code here:
        
        RV = JsldRED6.getValue();
        JpanelRGB.setBackground(new java.awt.Color(RV=JsldRED6.getValue(),GV=JsldGREEN6.getValue(),BV=JsldBLUE6.getValue()));
    }//GEN-LAST:event_JsldRED6JsldREDStateChanged

    private void JsldGREEN6JsldGREENStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_JsldGREEN6JsldGREENStateChanged
        // TODO add your handling code here:
        
        GV = JsldGREEN6.getValue();
        JpanelRGB.setBackground(new java.awt.Color(RV=JsldRED6.getValue(),GV=JsldGREEN6.getValue(),BV=JsldBLUE6.getValue()));
    }//GEN-LAST:event_JsldGREEN6JsldGREENStateChanged

    private void JsldBLUE6JsldBLUEStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_JsldBLUE6JsldBLUEStateChanged
        // TODO add your handling code here:
        
        BV = JsldBLUE6.getValue();
        JpanelRGB.setBackground(new java.awt.Color(RV=JsldRED6.getValue(),GV=JsldGREEN6.getValue(),BV=JsldBLUE6.getValue()));
    }//GEN-LAST:event_JsldBLUE6JsldBLUEStateChanged

    private void JbtnRGB1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JbtnRGB1ActionPerformed

        UsuarioN=JTF.getText();
        ContraN=JPF.getText();
        
        // TODO add your handling code here:
    }//GEN-LAST:event_JbtnRGB1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EditFRM.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EditFRM.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EditFRM.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EditFRM.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditFRM().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField JPF;
    private javax.swing.JTextField JTF;
    private javax.swing.JButton JbtnRGB;
    private javax.swing.JButton JbtnRGB1;
    private javax.swing.JPanel JpanelRGB;
    private javax.swing.JSlider JsldBLUE6;
    private javax.swing.JSlider JsldGREEN6;
    private javax.swing.JSlider JsldRED6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel23;
    // End of variables declaration//GEN-END:variables
}
